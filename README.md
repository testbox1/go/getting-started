# Getting Started

This Repo is to learn the basics of the Go programming language

- Write some simple "Hello, world" code.
- Use the go command to run your code.
- Use the Go package discovery tool to find packages you can use in your own code.
- Call functions of an external module

----
[Go Getting Started](https://go.dev/doc/tutorial/getting-started)
