# Getting Started

This Repo is to learn the basics of the Go programming language

- Write some simple "Hello, world" code.
- Use the go command to run your code.
- Use the Go package discovery tool to find packages you can use in your own code.
- Call functions of an external module

## 1. Write some code

```bash
# create directory to store code
mkdir hello

# initialize current directory as a go module. this i nesseccary to track dependencies 
go mod init example/hello

# create a file to store code for this module
touch hello.go
```

```hello.go
// Declare a main package (a package is a way to group functions, and it's made up of all the files in the same directory). 
package main	

// package that contain functions for formatting text, including printing to the console
import "fmt"

func main() {
	fmt.Println("Hello World")
}
```

```bash
# run code
go run .
```

## 2. Call code in external package

Add an external library to your project.

```go
package main

import "fmt"
// import external package from package manager
import "rsc.io/quote"

func main() {
    fmt.Println(quote.Go())
}
```

Get that specific library or manage all missing libraries for your go module.

```bash
go mod tidy
# or to get on specific package
go get rsc.io/quote
```

```bash
# run code
go run .
```





[Go Getting Started](https://go.dev/doc/tutorial/getting-started)
